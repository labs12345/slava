package com.ds;

import com.ds.chart.LineChart;
import com.ds.form.UploadFromFileSystemFrame;
import com.ds.form.UploadFromInternetFrame;
import org.jfree.ui.RefineryUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Выбор загрузки файла");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 300);
        JButton button1 = new JButton("из интернета");
        JButton button2 = new JButton("из файла");

        final JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(button1);
        panel.add(button2);

        frame.add(panel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        button1.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                new UploadFromInternetFrame();
            }
        });

        button2.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                UploadFromFileSystemFrame upload = new UploadFromFileSystemFrame();
                int result = upload.showOpenDialog(panel);
                if (result == JFileChooser.APPROVE_OPTION) {
                    try {
                        Scanner scanner = new Scanner(upload.getSelectedFile());
                        createDiagramFromFile(scanner);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    public static void createDiagramFromFile(Scanner scanner) {

        ArrayList<Double> after = new ArrayList<>();

        while (scanner.hasNext()) {
            try {
                after.add(Double.parseDouble(scanner.nextLine()));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
                after.add(0.0);
            }
        }
        System.out.println("Input date: ");
        after.forEach(item -> System.out.print(item + " "));

        ArrayList<Double> before = new ArrayList<>();

        SimpleMovingAverage movingAverage = new SimpleMovingAverage(3);
        for (double item : after) {
            movingAverage.addData(item);
            before.add(movingAverage.getMean());
        }

        System.out.println();
        System.out.println();
        System.out.println("Data for checking: ");
        before.forEach(item -> System.out.print(item + " "));

        LineChart lineChart = new LineChart("SMA (before)", after, before);
        lineChart.pack();
        RefineryUtilities.centerFrameOnScreen(lineChart);
        lineChart.setVisible(true);
        scanner.close();


    }

}
