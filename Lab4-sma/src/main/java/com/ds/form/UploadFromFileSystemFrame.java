package com.ds.form;

import javax.swing.*;

public class UploadFromFileSystemFrame extends JFileChooser{

    public UploadFromFileSystemFrame(){

        setDialogTitle("Выбор файла");
        setFileSelectionMode(JFileChooser.FILES_ONLY);
    }
}
