package com.ds.chart;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;

import java.awt.*;
import java.util.ArrayList;

public class LineChart extends ApplicationFrame {
    public LineChart(String frameName, ArrayList<Double> after, ArrayList<Double> before) {
        super(frameName);
        JFreeChart lineChart = ChartFactory.createLineChart(
                frameName,
                "value",
                "period",
                createDataset(after, before),
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );
        ChartPanel chartPanel = new ChartPanel(lineChart);
        chartPanel.setPreferredSize(new Dimension(560, 367));
        setContentPane(chartPanel);
    }

    private DefaultCategoryDataset createDataset(ArrayList<Double> res, ArrayList<Double> before) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int i = 0; i < res.size(); i++) {
            dataset.addValue(res.get(i), "data", "" + i);
        }

        for (int i = 0; i < before.size(); i++) {
            dataset.addValue(before.get(i), "simple average", "" + i);
        }
        return dataset;
    }
}
