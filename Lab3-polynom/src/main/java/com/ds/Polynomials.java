package com.ds;

public class Polynomials {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("No arguments");
            System.exit(0);
        }

        String arg = args[0];
        String[] arguments = arg.split(",");

        double result = 0.00;
        StringBuilder expression = new StringBuilder();

        for (int i = 0; i < arguments.length; i++) {
            String argument = arguments[i];

            expression.append("1/").append(argument).append("*3");
            result += 1 / Double.parseDouble(argument) * 3;

            if (i != arguments.length - 1)
                expression.append(" + ");
        }

        System.out.println("Expression: " + expression);
        System.out.println("Result: " + result);
    }

}
